# Stage de build
FROM golang:1.21-alpine AS builder

# Installation des dépendances nécessaires
RUN apk add --no-cache git

# Installation de templ
RUN go install github.com/a-h/templ/cmd/templ@latest

# Configuration du répertoire de travail
WORKDIR /app

# Copie des fichiers de dépendances
COPY go.mod ./
COPY go.sum ./

# Installation des dépendances
RUN go mod download
RUN go mod tidy

# Copie du code source
COPY . .

# Génération des templates et build
RUN templ generate
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o website ./cmd/main.go

# Stage final
FROM alpine:latest

# Installation des certificats CA
RUN apk --no-cache add ca-certificates

# Labels pour la maintenance
LABEL maintainer="Noé Pételle"
LABEL version="1.0"
LABEL description="Site web personnel"

# Création d'un utilisateur non-root
RUN adduser -D -h /app appuser

WORKDIR /app

# Copie du binaire et des fichiers statiques avec la bonne structure
COPY --from=builder /app/website .
COPY --from=builder /app/static ./static

# Donner les bonnes permissions
RUN chown -R appuser:appuser /app

USER appuser

# Configuration du port et des variables d'environnement
ENV DOCKER=true
EXPOSE 8080

# Healthcheck
HEALTHCHECK --interval=30s --timeout=3s \
    CMD wget --no-verbose --tries=1 --spider http://localhost:8080/ || exit 1

# Commande de démarrage
CMD ["./website"]
