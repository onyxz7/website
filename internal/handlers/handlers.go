package handlers

import (
	"net/http"
	"website/templates/pages"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	component := pages.Home()
	component.Render(r.Context(), w)
}

func CVHandler(w http.ResponseWriter, r *http.Request) {
	component := pages.CV()
	component.Render(r.Context(), w)
}
