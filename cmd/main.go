package main

import (
	"log"
	"net/http"
	"website/internal/handlers"
)

func main() {
	// Fichiers statiques
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	// Routes
	http.HandleFunc("/", handlers.HomeHandler)
	http.HandleFunc("/cv", handlers.CVHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
