# Site Personnel

Site web personnel minimaliste construit avec Go et Templ.

## Caractéristiques

- 🚀 Rapide et léger
- 🌓 Mode sombre/clair
- 🔄 Rechargement automatique en développement
- 🐳 Conteneurisation Docker

## Quick Start

```bash
# Cloner le repository
git clone https://gitlab.com/npetelle/website.git
cd website

# Lancer en développement
./dev.sh

# Ou construire pour la production
./dev.sh build
```

Pour plus de détails sur le développement, consultez la [documentation](docs/DEVELOPMENT.md).
