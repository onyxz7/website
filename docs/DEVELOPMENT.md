# Guide de Développement

## 🛠 Outils de Développement

Le script `dev.sh` est l'outil principal pour le développement. Il gère la compilation, le rechargement automatique et le déploiement.

### Prérequis

- Go 1.21+
- Docker (optionnel, pour le déploiement)

### Commandes Disponibles

```bash
./dev.sh [command]
```

| Commande | Description |
|----------|-------------|
| `dev`    | Lance le serveur de développement avec rechargement automatique |
| `build`  | Compile l'application pour la production |
| `docker` | Construit l'image Docker |
| `run`    | Lance l'image Docker |
| `clean`  | Nettoie les fichiers générés |
| `help`   | Affiche l'aide |

### Mode Développement

Le mode développement (`./dev.sh` ou `./dev.sh dev`) offre :

- 🔄 Rechargement automatique lors des modifications
- ⚡ Génération automatique des templates Templ
- 📝 Logs en temps réel
- 🎯 Compilation incrémentale

### Structure des Fichiers

```
website/
├── cmd/
│   └── main.go          # Point d'entrée
├── internal/
│   └── handlers/        # Gestionnaires HTTP
├── static/
│   ├── css/            # Styles
│   └── js/             # Scripts
├── templates/
│   ├── layout/         # Templates de mise en page
│   └── pages/          # Templates de pages
├── docs/
│   └── DEVELOPMENT.md  # Documentation
├── .air.toml           # Configuration Air (généré)
├── .gitignore
├── dev.sh             # Script de développement
├── Dockerfile
└── README.md
```

### Workflow de Développement

1. Lancer le serveur de développement :
   ```bash
   ./dev.sh
   ```

2. Le serveur démarre sur `http://localhost:8080`

3. Modifiez les fichiers :
   - Les fichiers `.go` sont recompilés automatiquement
   - Les fichiers `.templ` sont regénérés et recompilés
   - Les changements sont visibles immédiatement dans le navigateur

4. Pour arrêter le serveur : `Ctrl+C`

### Construction pour la Production

1. Build local :
   ```bash
   ./dev.sh build
   ```
   Crée un binaire dans `bin/website`

2. Build Docker :
   ```bash
   ./dev.sh docker
   ```
   Crée une image Docker optimisée

### Nettoyage

Pour nettoyer les fichiers générés :
```bash
./dev.sh clean
```

Cette commande supprime :
- Le dossier `bin/`
- Le dossier `tmp/`
- Les fichiers `*_templ.go` générés

### Bonnes Pratiques

- 🔍 Vérifiez les modifications avant de commit
- 🧹 Utilisez `./dev.sh clean` avant un commit majeur
- 📝 Gardez les templates Templ simples et modulaires
- 🎨 Testez les changements en mode clair et sombre

### Résolution des Problèmes

Si vous rencontrez des erreurs :

1. Vérifiez que Go et les dépendances sont installés
2. Nettoyez avec `./dev.sh clean`
3. Redémarrez le serveur de développement

Pour plus d'aide, consultez les logs ou ouvrez une issue sur GitLab.
