#!/bin/bash

# Couleurs pour les messages
GREEN='\033[0;32m'
BLUE='\033[0;34m'
RED='\033[0;31m'
NC='\033[0m'

# Fonction pour le nettoyage à la sortie
cleanup() {
    echo -e "\n${BLUE}Arrêt des processus...${NC}"
    kill $(jobs -p) 2>/dev/null
}

trap cleanup EXIT

# Fonction pour afficher l'aide
show_help() {
    echo "Usage: ./dev.sh [command]"
    echo ""
    echo "Commands:"
    echo "  dev       - Démarre le serveur de développement avec rechargement automatique"
    echo "  build     - Build l'application"
    echo "  docker    - Build l'image Docker"
    echo "  run       - Lance l'image Docker"
    echo "  clean     - Nettoie les fichiers générés"
    echo "  help      - Affiche cette aide"
}

# Fonction pour le développement
dev_server() {
    # Installation des dépendances Go si nécessaire
    if ! command -v templ &> /dev/null; then
        echo -e "${BLUE}Installation de Templ...${NC}"
        go install github.com/a-h/templ/cmd/templ@latest
    fi

    if ! command -v air &> /dev/null; then
        echo -e "${BLUE}Installation de Air...${NC}"
        go install github.com/cosmtrek/air@latest
    fi

    # Création du fichier de configuration air si nécessaire
    if [ ! -f .air.toml ]; then
        cat > .air.toml << 'EOL'
root = "."
tmp_dir = "tmp"

[build]
cmd = "templ generate && go build -o ./tmp/main ./cmd/main.go"
bin = "tmp/main"
include_ext = ["go", "templ"]
exclude_dir = ["tmp", "vendor", "bin"]
exclude_file = ["*_templ.go"]
exclude_regex = ["_templ\\.go$"]
delay = 1000
stop_on_error = true
send_interrupt = true
kill_delay = "0.5s"

[watch]
directories = [".", "cmd", "internal", "templates"]
exclude_files = ["*_templ.go"]

[log]
time = true

[color]
main = "magenta"
watcher = "cyan"
build = "yellow"
runner = "green"

[misc]
clean_on_exit = true
EOL
    fi

    # Génération initiale des templates
    echo -e "${GREEN}Génération initiale des templates...${NC}"
    templ generate

    # Démarrage du serveur de développement
    echo -e "${GREEN}Démarrage du serveur de développement...${NC}"
    DEBUG=false air
}

# Fonction pour build
build() {
    echo -e "${BLUE}Building application...${NC}"
    templ generate
    go build -o bin/website ./cmd/main.go
    echo -e "${GREEN}Build complete: bin/website${NC}"
}

# Fonction pour build Docker
docker_build() {
    echo -e "${BLUE}Building Docker image...${NC}"
    docker build -t website .
    echo -e "${GREEN}Docker build complete${NC}"
}

# Fonction pour run Docker
docker_run() {
    echo -e "${BLUE}Running Docker container...${NC}"
    docker run -p 8080:8080 website
}

# Fonction pour nettoyer
clean() {
    echo -e "${BLUE}Cleaning generated files...${NC}"
    rm -rf bin tmp
    rm -f bin .air.toml
    find . -name "*_templ.go" -delete
    echo -e "${GREEN}Clean complete${NC}"
}

# Gestion des commandes
case "$1" in
    "dev"|"")
        dev_server
        ;;
    "build")
        build
        ;;
    "docker")
        docker_build
        ;;
    "run")
        docker_run
        ;;
    "clean")
        clean
        ;;
    "help"|"-h"|"--help")
        show_help
        ;;
    *)
        echo -e "${RED}Commande inconnue: $1${NC}"
        show_help
        exit 1
        ;;
esac
